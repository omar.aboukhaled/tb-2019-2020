---
version: 1
titre: Implementation d'un jeu de gestion (Cinéma) 
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Julien Tscherrig
attribué à: [Guillaume Devaud]
mots-clés: [Simulation, Unity 3D, Gestion, Jeu, DOTS]
langue: [F]
confidentialité: non
suite: oui
---


## Contexte

Le projet consiste en la réalisation d’un jeu de gestion et stratégie. Son contexte est autour de la création et gestion d’un cinéma. Les contrôles possibles par le joueur sont variés et détaillés. Il pourra contrôler dans les détails la planification horaire de tous les éléments du jeu (employés, films). Il pourra engager des employés et les assigner à des postes de travail (vente de tickets, vente de nourriture, contrôle de tickets...). De plus, il contrôlera le prix de tous les éléments vendus aux clients. Le joueur aura pour but de faire accroitre le nombre de clients, tout en prenant en compte les demandes grandissantes.
Ce projet se déroule sur les bases d’un projet de semestre. Ce précédent a mis en place les bases d’un éditeur de carte et du comportement des clients. Il servira de base à l’ajout des nouvelles fonctionnalités.



## Objectifs

Les buts de ce projet sont les suivants :

- À partir des éléments existants, créez des systèmes permettant d’ajouter des éléments gestion.
  + Un système permettant de contrôler la planification des projections et des horaires de travail.
  + Un système monétaire pour l’achat de matériaux et la vente de nourriture et tickets.
  + Un système de ressources diverses pour l’achat et la maintenance des différents éléments du cinéma.
  + Des personnages intelligents possédant un certain nombre de caractéristiques définissant leurs capacités à effectuer les tâches qui leur seront assignées.
  + Un système de sauvegarde permettant au joueur de retourner à un point passé et de ne pas perdre son avancée lors de la fermeture du jeu.
- La structure du code doit être aussi modulable que possible afin de rendre l’évolution et l’ajout de nouveaux éléments simple à faire.
- Créer un jeu jouable qui pourrait être publié et vendu si on fait abstraction de la partie visuelle et sonore.

## Contraintes

Les contraintes du projet sont les suivantes :

- Utilisation d’Unity, un moteur de jeu multiplateforme mettant en avant la rapidité de prototypage et sa complexité convenant aux débutants comme avancés.
- Utilisation de la Data-Oriented Technology Stack, une librairie développée par Unity permettant de prendre avantage du multithreading. Il utilise le paradigme de la programmation orientée donnée pour y arriver.
- Tous les éléments du jeu doivent être les plus modulables possible. Ainsi, l’ajout, la modification et la réutilisation des éléments du jeu sont facilités.
- Les performances du jeu sont d’une importance cruciale. Car celui-ci se devrait d’être jouable sur une grande variété d’ordinateurs.



