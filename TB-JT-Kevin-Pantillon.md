---
version: 1
titre: Escape Room en réalité virtuelle avec Hand Tracking
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Julien Tscherrig
mots-clé: [Simulation, Unity 3D, Gestion, Jeu, Occulus, HTC, VR, AR]
langue: [F]
attribué à: [Kevin Pantillon] 
confidentialité: oui
suite: oui
---

## Description
Après l’exploration de la technologie de la réalité virtuelle avec l’Oculus Quest lors du projet de semestre 5, ainsi que le développement de nouveaux outils de Hand Tracking lors du projet de semestre 6, ce travail de Bachelor s’inscrit dans la continuité en mettant en application les compétences et outils implémentés précédemment afin de réaliser une application de type Escape Room.
Chaque pièce du jeu contiendra des énigmes qui devront être résolues avant de passer au niveau suivant. Ces épreuves tireront parti des nouvelles possibilités offertes par la technologie du Hand Tracking. L’expérience créée permettra une immersion totale du joueur.

## Objectifs
Les buts de ce projet sont les suivants :

Objectifs à atteindre 
- Création de plusieurs types d’énigmes
- Réalisation d’une Escape Room en réalité virtuelle utilisant le Hand Tracking

## Contraintes

Les contraintes du projet sont les suivantes :

-	Équipement : Oculus Quest (Hand Tracking)
-	Technologie : Unity

