---
version: 1
titre: Synchronization of physiological signals with facial expressions 
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Karl Daher
instituts: [HumanTech]
mots-clé: [Emotion detection, Physiological signals, Facial expressions, Micro expressions]
langue: [F, E]
confidentialité: oui
suite: non
---

## Description

Nowadays machines work really well when taking commands and applying it. Yet these machines do not have any emotional response or empathic comprehension of a situation. 
At the HumanTech institute, under Empathic Labs we are researching and developing empathic systems, empathic systems are applications and software that analyze detect understand and respond emotionally to humans. 

Two of the important metrics in empathic systems are facial expressions because it can express the human feelings, physiological signals because it can be analyzed to detect emotions the person is going through.

The project consists of researching already existing systems and algorithms, developing a prototype in order to synchronize physiological signals and facial expressions.
After that, we would like to analyze the results we are having which can come as a secondary goal.

## Tasks

- Initialize the variable and specifications of the prototype
- Analyze the existing systems and tools
- Design and implement of the system prototype
- Evaluate the prototype
- Write the adequate documentation

```{=tex}
\begin{center}
\includegraphics[width=0.45\textwidth,height=\textheight]{img/ht.png}
\end{center}
```

```{=tex}
\begin{center}
\includegraphics[width=0.4\textwidth,height=\textheight]{img/logo_Labs_trans.png}
\end{center}
```
