---
version: 1
titre: NaturaGame - II 
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Julien Tscherrig
attribué à: [Kevin Manixab]
mots-clés: [Simulation, Unity 3D, Gestion, Jeu, DOTS]
langue: [F]
confidentialité: non
suite: oui
---
## Contexte

NaturaGame est un projet du laboratoire d'Innovation Pédagogique de l'Unifr en partenariat avec le musée de la nature de Sion. 
Le but de ce projet est de développer un jeu pédagogique s'adressant à des élèves de 12 à 15 ans venant visiter le musée dans un cadre scolaire afin de leur apprendre ce qu'est l'anthropocène. Le jeu est divisé en deux parties avec deux jeux différents.

## Objectifs

Les buts de ce projet sont les suivants :

- Développer le scan de QR code qui permet de scanner les animaux pour ensuite permettre les différentes actions.
- Développer les jeux (fonctionnalités du jeu, système de ressources, inventaire, actions, etc.).
- Développer la partie multijoueurs avec la connexion entre tablettes pour les échanges des ressources.

