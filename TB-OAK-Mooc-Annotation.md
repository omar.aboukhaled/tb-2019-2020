---
version: 1
titre: Système d'annotation MOOC
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - JOE-SI
professeurs co-superviseurs:
  - Julien Tscherrig
mots-clé: [Développement Web, interface responsive, MOOC, vidéo]
langue: [F]
confidentialité: oui
instituts: [HumanTech]
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth,height=\textheight]{img/TB-OAK-Mooc-Annotation.png}
\end{center}
```

## Description
Il s’agit de développer une application web responsive permettant d’interagir avec une vidéo d’une présentation d’un cours dispensé en mode MOOC (Massive Open Online Courses) – illustration plus bas.

## Objectifs/Tâches
Pour l’enseignant, l’application doit lui permettre entre autres : 
- De visualiser de manière synchronisée la vidéo avec des affichages des slides clés (ex. changement de slides, etc.).
- D’augmenter la vidéo pour l’ajout de méta-information, comme une table de matière, etc. 
- Le rajout des commentaires et des hyperliens vers des ressources externes.

Pour les étudiants, l’application doit leurs permettre entre autres : 
- De visualiser la vidéo augmentée
- De rajouter des commentaires personnels sur des moments clés des slides
- De partager ces commentaires avec ses collègues de classe
