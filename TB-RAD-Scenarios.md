---
version: 1
titre: Prototypage de Scénarios de Jeux 
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Maurizio Rigamonti
  - Julien Tscherrig
mots-clé: [Simulation, Unity 3D, Gestion, Jeu, DOTS]
langue: [F]
confidentialité: oui
suite: non
---
## Contexte

De nos jours, la ludification (gamefication) a gagné en importance pour la formation du personnel et des cadre dans les entreprises. 
Bien que les méthodes de design et les outils aient évolué dans le temps et permettent aujourd’hui d’accélérer les processus de prototypage, la création de scénarios est une tâche très complexe parce qu’elle demande des compétences techniques avancées et souvent interdisciplinaires. L’implication de cette complexité est la difficulté de pouvoir développer des prototype rapides avant de démarrer le vrai processus de production, avec les risques associés en termes de réussite et coûts.

## Objectifs

Le but de ce projet est de construire un système de prototypage rapide de scénarios, basée sur des outils existants. Plus précisément, plusieurs problématiques doivent être résolues dans le cadre du projets, dont les buts :
- Définir les éléments qui définissent un scénario (narration, buts, règles, séquences des événements, actions).
- Concevoir un format de représentation du scénario.
- Développer des outil interactifs qui permettent de construire un scénario de manière visuelle et de le sauvegarder/charger
- Valider la conception et l’intégration avec un use case.

L’application sera validé par la création effectives de scénarios pour une entreprise.

## Contraintes

Les contraintes du projet sont les suivantes :

Le projet vise à éteindre le logiciel Unity et le développement se fera en C#.


