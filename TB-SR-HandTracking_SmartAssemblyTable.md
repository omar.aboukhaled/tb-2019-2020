---
version: 1
titre: Système de tracking des mains pour table de montage intelligente
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - Lino Peverada
professeurs co-superviseurs:
  - Simon Ruffieux
mots-clé: [Image Processing, Hand Tracking, Smart Assembly Workspace, Machine Learning]
langue: [F]
confidentialité: oui
instituts: [HumanTech]
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth,height=\textheight]{img/TB-SR-HandTracking_SmartAssemblyTable_1.png}
\end{center}
```

## Description

L'entreprise PMF-System SA basée à Marly produit des meubles spécialement pensés pour les entreprises. Elle produit notamment des tables de montage (Assembly Workspace) pour diverses PME locales et cherche à rendre ces tables intelligentes. 
L'utilisation d'une caméra  permettra de suivre les actions effectuées par l'opérateur (prendre une pièce, déposer une pièce) afin de s'assurer que tous les étapes de l'assemblage sont réalisées dans le bon ordre. 
La table de montage sera composée d'une caméra, d'une lampe et d'un projecteur (tous installés au-dessus de la table et pointant vers le bas (top-view)). 
Le projecteur sera utilisé (dans une prochaine étape du projet) pour fournir des informations à l'utilisateur directement sur l'espace de travail et potentiellement pour mettre en valeur certaines parties du montage en cours de réalisation. 
A terme, ce système a pour but de simplifier le contrôle qualité des produits réalisés et d'aider les opérateurs à apprendre de nouveaux scénarios de montage.

Ce projet d'étudiant représente la première étape du projet: permettre de suivre les actions de l'opérateur en temps-réel. Il peut potentiellement être réalisé par 2 étudiants en adaptant le cahier des charges. 

## Contraintes (négociables)

- Le système doit permettre de suivre automatiquement la position des mains d'un utilisateur
- Le système doit être capable de détecter la prise et le dépot d'une pièce
- Le système doit être capable de détecter si une suite d'opérations correspond à un scénario pré-défini
- L'interface utilisateur doit permettre de définir des zones de prise de pièce (au moins 3 zones)
- L'interface utilisateur doit permettre de définir des zones de dépose de pièce (1 zone)
- L'interface utilisateur doit permette de visualiser la position des mains en temps réel et la suite d'opérations effectuées.
- Le language n'est pas restreint mais nous proposons d'utiliser Python ou C++
- Le hardware sera composé de: 1 caméra RGB, 1 lampe, 1 table de montage (encore à discuter avec le partenaire du fait des conditions actuelles particulières)
- Le système sera implémenté sur un Jetson Nano de Nvidia (à discuter)
- Le système doit être modulaire afin de simplifier son intégration future

## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Analyse détaillée des frameworks, des langages et des outils, puis choix de la solution la plus appropriée pour le système
- Réalisation des algorithmes de tracking et d'une interface utilisateur de débuggage 
- Tests et validation du système
- Rédaction d'une documentation adéquate