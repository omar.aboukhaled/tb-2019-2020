---
version: 1
titre: BombusCar 3 - Optimisation avancée d’un réseau de distribution
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Francesco Carrino
attribué à: [Quentin Seydou]
mots-clé: [Python, Optimisation, Particle Swarm Optimizer, Algorithmes Génétiques]
langue: [F,E]
confidentialité: oui
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth,height=\textheight]{img/TB_FC_BombusCar_3.png}
\end{center}
```

## Contexte

Le coût environnemental et sociétal de la logistique traditionnelle, basée sur le modèle « Hub and spoke », est de plus en plus défavorable. La digitalisation permet de nouvelles approches qui ont montré leur efficacité dans le transport de personne (p.ex. BlaBlaCar) mais il reste une opportunité à saisir dans le domaine du transport de marchandises.
Ce projet propose la création d’un réseau de distribution intelligent, flexible et sécurisé pour le transport de colis en exploitant des capacités de transport déjà existantes.

En l’état actuel, des algorithmes de type « greedy » sont en cours d’implémentation et test. Ceux-ci permettent de trouver le chemin optimal pour chaque colis considéré de manière individuelle. Toutefois ils ne permettent pas de considérer à la fois l’ensemble des besoins du réseau.  En outre, l’implémentation courante considère le coût uniquement en termes du temps de livraison à minimiser.  

Le but de ce projet de bachelor est de trouver le parcours optimal pour l’ensemble des colis dans le système à partir du graphe du réseau. L’étudiant analysera, implémentera (Python) et testera plusieurs algorithmes d’optimisation comme Particle Swarm Optimizer et des algorithmes génétiques.



## Objectifs

- Objectifs principaux :
	+ Analyse des algorithmes d’optimisation les plus adaptés au problème (routing)
	+ Implémentation et test des algorithmes sélectionnés qui prennent également en compte dans l’optimisation un plus large éventail de paramètres et contraintes (p. ex. la capacité des véhicules, l’empreinte carbone, etc.)
- Objectif secondaire :
	+ Intégration de la sortie (output) de l’algorithme de routage dans le backend du système « bombus car ». 



## Tâches

1. Analyse – Analyse des algorithmes d’optimisation
2. Conception 
	a. Mise à jour de l’architecture du système 
	b. Définition d’une ou plusieurs fonctions de coût à minimiser
3. Développement et test - Comparaisons des algorithmes en termes d’efficacité (réduction du cout global) et efficience (temps de calcul)