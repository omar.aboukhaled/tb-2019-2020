---
version: 1
titre: Teacher Co-Pilot – Application en VR pour améliorer l’enseignement 
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Francesco Carrino
  - Lionel Alvarez (HEP Fribourg)
attribué à: [Dorian Saudan]
mots-clé: [Réalité Virtuelle, Simulation, Unity 3D, données physiologiques, données environnementales]
langue: [F,E]
confidentialité: oui
suite: oui
---


## Contexte

Une salle de classe est un environnement complexe et de nombreux facteurs peuvent affecter la qualité de l'enseignement. Le projet « Teacher Co-Pilot » vise à utiliser les nouvelles technologies (c.-à-d. réalité virtuelle, apprentissage automatique, caméras 360 ° / 3d, capteurs environnementaux et physiologiques) pour surveiller une classe et ses différentes composantes (par exemple, l'enseignant, les élèves, la qualité de l'air, le niveau de bruit, etc.). Ces informations seront traitées et présentées de manière efficace à un « copilote ». Le copilote est une personne (ou un groupe de personnes) qui suit en temps réel, à distance, le cours de la classe. Grâce à un casque VR, ils pourront avoir une visualisation 3D de la salle de classe complète et de l'enseignant enrichie par les informations liées à tous les paramètres environnementaux et physiologiques surveillés. Le copilote sera en mesure de communiquer de fournir des suggestions en temps réel (par exemple, « ouvrez les fenêtres », « essayez de varier le ton de votre voix plus souvent », « Les élèves de la dernière rangée sont distraits », etc.). 



## Objectifs

Le but de ce projet de bachelor est d’implémenter une première version du système que puisse afficher en temps réel en réalité virtuelles, l’état d’une classe.

- Objectifs principaux :
	+ Simulation simplifiée d’une classe et des facteurs principaux qui peuvent affecter la qualité de l'enseignement
	+ Visualisation ergonomique et intuitives des données physiologiques et environnementales (simulées)
- Objectif secondaire :
	+ Intégration de données provenant de capteurs réels (p. ex, en utilisant Arduino/Rasoberry Pi) de signaux comme capteurs de CO2, température



## Tâches

1. Analyse :
	a. Contraintes et besoins pour le monitoring d’une classe 
	b. Prise en main des outils (VR headset, Unity 3D, etc.)
2. Conception 
	a. Architecture modulaire du système pour simplifier l’ajoute de nouveaux capteurs (virtuels ou réels)
	b. Maquettes des interfaces Copilote et Enseignant
3. Développement et test



